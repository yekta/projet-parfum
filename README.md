À faire
------------
```
Un échantillion doit être un produit et on doit pouvoir référencer un produit à un produit échantillion (en bdd rajouter un attribut boolean echantillon) s'il est à true on peut le lié aux produits
sinon non. Dans la page edit produit on aura la liste des échantillions pour pouvoir la lié au produit.
En cas d'ajout au panier check si l'utilisateur n'a pas plus de 5 échantillions dans le panier si oui on affiche une erreur car limiter à 5 durant une période de 30jours ou entre 2 commandes.
remise  la prochaine commande si celle-ci se fait moins d’un mois après la commande des échantillons.
```
- [x] un attribut boolean dans la table product pour savoir si c'est un echantillon ou pas (je n'ai pas réussi à faire fonctionner le formulaire)
- [ ] lié un échantillon à un produit.
Un échantillon coûte 50 centimes.
- [ ] Il n’est possible de commander que 5 échantillons entre deux commandes. Le compteur est malgré tout remis à 0 si la dernière commande d’échantillon date d’il y a plus de 30 jours
- [ ] Le prix des échantillons est remboursé sur la prochaine commande si celle-ci se fait moins d’un mois après la commande des échantillons

Howto
------------
Commandes utiles:
```bash
composer install #installer le site
yarn install #installer les assets dépendences
yarn build #builder les assets
php bin/console sylius:install #installer la configuration(bdd,currency...)
php bin/console server:run --docroot=web #lancer le serveur
composer cache:clear --no-warmup #nettoyer le cache
```

Troubleshooting
---------------

If something goes wrong, errors & exceptions are logged at the application level:

```bash
$ tail -f var/logs/prod.log
$ tail -f var/logs/dev.log
```

If you are using the supplied Vagrant development environment, please see the related [Troubleshooting guide](etc/vagrant/README.md#Troubleshooting) for more information.


<?php

declare(strict_types=1);

namespace App\Form\Extension;

use Sylius\Bundle\ProductBundle\Form\Type\ProductType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
// use App\Form\Type\ProviderEntityType;

final class ProductTypeExtension extends AbstractTypeExtension
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            // Adding new fields works just like in the parent form type.
            ->add('echantillon', CheckboxType::class, [
                'required' => false,
                'label' => 'sylius.form.product.echantillon',
            ])
            ;
    }

    public static function getExtendedTypes(): iterable
    {
        return [ProductType::class];
    }
}
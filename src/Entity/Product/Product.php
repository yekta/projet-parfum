<?php

declare(strict_types=1);

namespace App\Entity\Product;

use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Core\Model\Product as BaseProduct;
use Sylius\Component\Product\Model\ProductTranslationInterface;

/**
 * @Entity
 * @Table(name="sylius_product")
 */
class Product extends BaseProduct
{
    /** @ORM\Column(type="boolean") */
    private $echantillon;

    public function getEchantillon(): ?boolean
    {
        return $this->echantillon;
    }

    public function setEchantillon(boolean $echantillon): self
    {
        $this->echantillon = $echantillon;
    }

    protected function createTranslation(): ProductTranslationInterface
    {
        return new ProductTranslation();
    }
}
